document.getElementsByClassName("wrapper")[0].addEventListener("click", openTab);
document.getElementsByClassName("wrapper")[1].addEventListener("click", openTab);

function openTab() {

    tab = document.getElementsByClassName("tab")[0];

    if(tab.style.display == "none"){
        tab.style.display = "block";

        container = document.getElementsByClassName("container")[0];
        container.style.width = "65%";
        container.style.marginLeft = "32.5%";

    }else{
        tab.style.display = "none";

        container = document.getElementsByClassName("container")[0];
        container.style.width = "90%";
        container.style.marginLeft = "auto";

    }  
  
}
  // Get the element with id="defaultOpen" and click on it
  //document.getElementById("defaultOpen").click();