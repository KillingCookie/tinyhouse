<?php

require_once('AppController.php');
require_once(__DIR__ ."\..\models\User.php");
require_once(__DIR__ ."\..\\repository\UserRepository.php");

class LoginController extends AppController {

    public function login(){

        $userRepository = new UserRepository();

        if ($this->isPost()) {//check button //hash password

            if(isset($_POST['login'])){
            $email = $_POST['email'];
            $password = $_POST['password'];

            $user = $userRepository->getUser($email);

                if ($user == null) {
                    $this->render('login', ['messages' => ['No user with this email!']]);
                }

                if (!password_verify($password, $user->getPassword())) {
                    $this->render('login', ['messages' => ['Wrong password!']]);
                }

                $_SESSION["id"] = $user->getEmail();
                $_SESSION["role"] = $user->getRole();

                $url = "http://$_SERVER[HTTP_HOST]/";
                header("Location: {$url}pai/?page=home");
                return;

            }else if(isset($_POST['signup'])){

                $url = "http://$_SERVER[HTTP_HOST]/";
                header("Location: {$url}pai/?page=register");
                return;

            }

        }

        $this->render('login');

    }

    public function register(){

        $userRepository = new UserRepository();

        if ($this->isPost() && isset($_POST['signup'])) {//check  //hash password

            $email = $_POST['email'];
            $password = $_POST['password'];
            $login = $_POST['login'];

            $user = $userRepository->getUser($email);

            if ($user != null) {
                $this->render('login', ['messages' => ['User with this email already exists!']]);
                return;
            }

            if ($password !== $_POST['repeat_password']) {
                $this->render('login', ['messages' => ['Password must be the same!']]);
                return;
            }

            $user = $userRepository->insertUser($email, password_hash($password, PASSWORD_BCRYPT), $login);

            $_SESSION["id"] = $user->getEmail();
            $_SESSION["role"] = $user->getRole();

            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}pai/?page=home");
            return;

        }

        $this->render('login');

    }

    public function logout(){

        session_unset();
        session_destroy();

        $url = "http://$_SERVER[HTTP_HOST]/";
        header("Location: {$url}pai/?page=login");

    }

}