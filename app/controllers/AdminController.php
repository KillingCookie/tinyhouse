<?php

require_once('AppController.php');
require_once(__DIR__ ."\..\models\User.php");
require_once(__DIR__ ."\..\\repository\UserRepository.php");

class AdminController extends AppController {

    private $users = [];

    public function admin(){

        $userRepository = new UserRepository();
        $retval = $userRepository->getUsers();

        if(isset($retval)){
                    
            while($row = $retval->fetch_assoc()){

                $user = new User($row['email'], $row['password'], $row['login'], [$row['role']]);
                array_push($this->users, $user);

            }

        }

        if ($this->isPost()) {//check button //hash password

            

            foreach($this->users as $key=>$user){

                if(isset($_POST['delete-user-'.$key])){
                    
                    $userRepository->deleteUser($user);

                }

            }

        }

        $this->render('admin', ['users' => $this->users]);

    }

}