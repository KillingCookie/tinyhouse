<?php

require_once('AppController.php');

class IndexController extends AppController {

    public function index(){

       if($this->isPost()){

            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/pai/?page=login");
            return;

        }

        $this->render('index');

    }

}