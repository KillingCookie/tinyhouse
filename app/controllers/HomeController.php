<?php

require_once('AppController.php');
require_once(__DIR__ ."\..\\models\Room.php");
require_once(__DIR__ ."\..\\repository\RoomRepository.php");
require_once(__DIR__ ."\..\\models\Service.php");
require_once(__DIR__ ."\..\\repository\ServiceRepository.php");

class HomeController extends AppController {

    private $rooms = [];
    private $services = [];
    private $thisRoom;

    public function home(){

        if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/pai/?page=index");
        }

        if(!isset($_GET['room'])){

            $this->rooms = [];

        }

        $roomRepository = new RoomRepository();
        $retval = $roomRepository->getRooms($_SESSION['id']);

        if(isset($retval)){
            
            while($row = $retval->fetch_assoc()){

                //if($row['name'] != "Room Name" && $row['image'] != "NewRoom");
                array_push($this->rooms, new Room($row['name'], $row['image'], $row['room_id']));

            }

        }

        if(isset($_GET['room'])){

            if($_GET['room'] != "new"){//type 0 == NEW service, NEW ROOM name == new room

                $this->thisRoom = $this->rooms[$_GET['room']];

                $serviceRepository = new ServiceRepository();
                $retval = $serviceRepository->getServices($_SESSION['id'], $this->thisRoom->getName());
                
                if(isset($retval)){
                    
                    while($row = $retval->fetch_assoc()){
    
                        //array_push($this->services, new Service($row['name'], $row['value'], $row['type']));
                        $this->thisRoom->addService(new Service($row['name'], $row['value'], $row['type'], $row['service_id']));
    
                    }
    
                }

            }else{

                $this->thisRoom = new Room("Room Name", "NewRoom");
                $this->thisRoom->insert();
                array_push($this->rooms, $this->thisRoom);

                //die(array_search($this->thisRoom, $this->rooms)."b");
                
                $url = "http://$_SERVER[HTTP_HOST]";
                header("Location: {$url}/pai/?page=home&room=".array_search($this->thisRoom, $this->rooms));
                //new empty object, which im going to add after save to db
                //CHANGE EDIT AND SAVE TO BUTTONS
                //$this->render('home', ['rooms' => $this->rooms, 'thisRoom' => null, 'services' => null]);
                //return;

            }

            if($this->isPost() && isset($_POST['edit-button'])){

                //if($_GET['room'] != "new"){//

                    //$this->services = $this->thisRoom->getServices();

                //}

                $url = "http://$_SERVER[HTTP_HOST]";
                header("Location: {$url}/pai/?page=home&room=".$_GET['room']."&edit=true");

            }

            if(isset($_GET['edit'])){

                $this->edit();
                return;

            }

            $this->render('home', ['rooms' => $this->rooms, 'thisRoom' => $this->thisRoom, 'services' => $this->thisRoom->getServices()]);
            return;

        }
        
        $this->render('home', ['rooms' => $this->rooms]);

    }

    public function edit(){

        //if($this->isPost()){

            if($_GET['room'] != 'new'){

                //die("smert");
                //if()
                $this->services = $this->thisRoom->getServices();

            }else{

                $this->services = [];

                //do something with new room, cause that is hard one
                //add new room to dbase as a temp one, if not seved delete it
                //do not render it as a different room

                //add all new services to db and update them after save/delete after not save

            }
                
            if(isset($_POST['add-service'])){

                //$this->thisRoom->addService(new Service(" ", " ", " "));
                array_push($this->services, new Service("new", "21", 0));//get it to database
                end($this->services)->addService($this->thisRoom);

            }else if(isset($_POST['delete'])){

                $this->thisRoom->delete();

                $this->thisRoom = null;
                $this->services = [];

                $url = "http://$_SERVER[HTTP_HOST]";
                header("Location: {$url}/pai/?page=home");

            
            }else if(isset($_POST['save'])){

                $oldName = $this->thisRoom->getName();
                $this->thisRoom->setName($_POST['room-name']);
                $this->thisRoom->setImage($_POST['room-image']);

                $servRepo = new ServiceRepository();

                foreach($this->services as $key=>$serv){

                    $serv->setName($_POST['service-name-'.$key]);
                    $serv->setValue($_POST['service-value-'.$key]);
                    if($_POST['service-type-'.$key] == 'number'){

                        $serv->setType(1);

                    }else if($_POST['service-type-'.$key] == 'percentage'){

                        $serv->setType(2);

                    }else if($_POST['service-type-'.$key] == 'temperature'){

                        $serv->setType(3);

                    }
                    if($serv->getType() == 0){
                        
                        $serv->delete();

                    }else{

                        $serv->updateService();

                    }

                }//$this->thisRoom->setServices($this->services);
            
                $this->thisRoom->update($oldName);

                $this->thisRoom = null;
                $this->services = [];

                $url = "http://$_SERVER[HTTP_HOST]";
                header("Location: {$url}/pai/?page=home&room=".$_GET['room']);//if new?

            }

            foreach($this->services as $key=>$serv){

                if(isset($_POST['service-delete-'.$key])){
                    
                    $serv->delete();

                }

            }

        //}

        $this->render('home', ['rooms' => $this->rooms, 'thisRoom' => $this->thisRoom, 'services' => $this->services]);
        return; 

    }

}