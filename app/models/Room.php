<?php

require_once("Service.php");
require_once(__DIR__."\..\\repository\RoomRepository.php");

class Room{

    private $name;
    private $services = [];
    private $imagePath;
    private $id;

    public function __construct(string $roomName, string $imgPath, int $id = 0, array $functionality = []){

        $this->name = $roomName;
        $this->imagePath = $imgPath;//whole path
        if(isset($functionality)){

            $this->services = $functionality;

        }
        if($id != 0){

            $this->id = $id;

        }

    }

    public function insert(){

        $roomRepo = new RoomRepository();
        $this->id = $roomRepo->insertRoom($this);

    }

    public function update(string $oldName){

        $roomRepo = new RoomRepository();
        $roomRepo->updateRoom($this, $oldName);

    }

    public function delete(){

        $roomRepo = new RoomRepository();
        $roomRepo->deleteRoom($this);

    }

    public function setServices(array $serv){

       $this->services = $serv;

    }

    public function addService(Service $serv){

        array_push($this->services, $serv);

    }

    public function getName() : string {

        return $this->name;

    }

    public function setName(string $name){

        $this->name = $name;

    }

    public function getServices() : array{

        return $this->services;

    }

    public function getImage() : string{

        return $this->imagePath;

    }

    public function setImage(string $img){

        $this->imagePath = $img;

    }

    public function setId(int $id){

        $this->id = $id;

    }

    public function getId() : int{

        return $this->id;

    }

}

?>