<?php

class User{

    private $email;
    private $password;
    private $login;
    private $role = [];
    private $rooms = [];

    public function __construct(string $email, string $password, string $login, array $role = ['ROLE_USER']) {

        $this->email = $email;
        $this->password = $password;
        $this->login = $login;
        $this->role = $role;

    }

    public function getLogin(): string {

        return $this->login;

    }

    public function getEmail(): string {

        return $this->email;

    }

    public function getPassword(){

        return $this->password;

    }

    public function getRole(): array{

        return $this->role;
        
    }

}

?>