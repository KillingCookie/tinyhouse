<?php

require_once("Room.php");
require_once(__DIR__."\..\\repository\ServiceRepository.php");

class Service{

    private $name;
    private $value;
    private $type;
    private $id;

    public function __construct(string $name, string $value, int $type, int $id = 0){

        $this->name = $name;
        $this->value = $value;//whole path
        $this->type = $type;

        if($id != 0){

            $this->id = $id;

        }

    }

    public function addService(Room $room){

        $servRepo = new ServiceRepository();
        $this->id = $servRepo->insertService($this, $room);//add id

    }

    public function updateService(){

        $servRepo = new ServiceRepository();
        $servRepo->updateService($this);

    }

    public function delete(){

        $servRepo = new ServiceRepository();
        $servRepo->deleteService($this);

    }

    public function getName() : string {

        return $this->name;

    }

    public function setName(string $name){

        $this->name = $name;

    }

    public function getValue(){

        return $this->value;

    }

    public function setValue(string $value){

        $this->value = $value;

    }

    public function getType() : int{

        return $this->type;

    }

    public function setType(int $type){

        $this->type = $type;

    }

    public function setId(int $id){

        $this->id = $id;

    }

    public function getId() : int{

        return $this->id;

    }

}

?>