<?php

require_once "Repository.php";
require_once __DIR__.'\..\models\User.php';
require_once __DIR__.'\..\..\Database.php';

class UserRepository extends Repository {

    //private $dbase = null;

    public function getUser(string $email): ?User {

        $sql = 'SELECT * FROM user WHERE email = "' . $email . '"';

        if($this->dbase == null){

            $dbase = new Database();
            $dbase->connect();
            $this->dbase = $dbase;

        }

        $retval = $this->dbase->select($sql);
        $row = $retval->fetch_assoc();

        if($row == null) {
            return null;
        }

        return new User($row['email'], $row['password'], $row['login'], [$row['role']]);
        
    }

    public function getRawUser(string $email) : array {

        $sql = 'SELECT * FROM user WHERE email = "' . $email . '"';

        if($this->dbase == null){

            $dbase = new Database();
            $dbase->connect();
            $this->dbase = $dbase;

        }

        $retval = $this->dbase->select($sql);
        $row = $retval->fetch_assoc();

        return $row;

    }

    public function deleteUser(User $user){

        $sql = 'DELETE FROM user WHERE (user.email = "'.$user->getEmail().'") AND
                                        (user.login = "'.$user->getLogin().'") AND
                                        (user.password = "'.$user->getPassword().'")';

        $this->dbase->query($sql);

        //die($sql);

    }

    public function insertUser(string $email, string $password, string $login): User{//check if user exists here

        $sql = 'INSERT INTO `user` (email, password, login) VALUES ("'.$email.'","'.$password.'","'.$login.'")';

        $this->dbase->query($sql);//check if ok

        return new User($email, $password, $login);

    }

    public function getUsers(){

        $sql = 'SELECT user.* 
                FROM user';

        if($this->dbase == null){

            $dbase = new Database();
            $dbase->connect();
            $this->dbase = $dbase;

        }$retval = $this->dbase->select($sql);

        return $retval;

    }

}

?>