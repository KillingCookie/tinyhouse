<?php

require_once "Repository.php";
require_once "UserRepository.php";
require_once __DIR__.'\..\models\User.php';
require_once __DIR__.'\..\models\Room.php';
require_once __DIR__.'\..\..\Database.php';

class RoomRepository extends Repository {

    //private $dbase = null;

    public function getRoom(string $email, string $name): ?Room {

        $sql = 'SELECT room.* 
                FROM room 
                INNER JOIN user ON room.user_id = user.user_id 
                WHERE (user.user_id = ( SELECT user.user_id WHERE user.email = "'.$email.'" )) 
                        AND (room.name = "'.$name.'")';

        if($this->dbase == null){

            $dbase = new Database();
            $dbase->connect();
            $this->dbase = $dbase;

        }

        $retval = $this->dbase->query($sql);
        $row = $retval->fetch_assoc();

        if($row == null) {
            return null;
        }

        return new Room($row['name'], $row['image']);
        
    }

    public function getRawRoom(string $email, string $name): array {

        $sql = 'SELECT room.* 
                FROM room 
                INNER JOIN user ON room.user_id = user.user_id 
                WHERE (user.user_id = ( SELECT user.user_id WHERE user.email = "'.$email.'" )) 
                        AND (room.name = "'.$name.'")';

        if($this->dbase == null){

            $dbase = new Database();
            $dbase->connect();
            $this->dbase = $dbase;

        }//die($sql);

        $retval = $this->dbase->select($sql);
        $row = $retval->fetch_assoc();
        return $row;

    }

    public function insertRoom(Room $room) : int{//check if user exists here

        $userRepo = new UserRepository();
        $user = $userRepo->getRawUser($_SESSION['id']);

        $sql = 'INSERT INTO `room` (user_id, name, image) VALUES ("'.$user['user_id'].'","'.$room->getName().'","'.$room->getImage().'")';

        $this->dbase->query($sql);

        $row = $this->getRawRoom($_SESSION['id'], $room->getName());

        return $row['room_id'];

        //insert SERVICES

    }

    public function deleteRoom(Room $room){//check if user exists here

        $sql = 'DELETE FROM room WHERE room.room_id = "'.$room->getId().'"';

        $this->dbase->query($sql);

    }

    public function updateRoom(Room $room, string $oldName){//check if user exists here

        $rawRoom = $this->getRawRoom($_SESSION['id'], $oldName);

        $sql = 'UPDATE `room` SET name = "'.$room->getName().'",
                                    image = "'.$room->getImage().'"
                                    WHERE room_id = "'.$rawRoom['room_id'].'"';

        $this->dbase->query($sql);//check if ok

        //update SERVICES

    }

    public function getRooms(string $email) {

        $sql = 'SELECT room.*
                FROM room
                INNER JOIN user ON room.user_id = user.user_id
                WHERE user.user_id = ( SELECT user.user_id WHERE user.email = "'.$email.'" )';

        if($this->dbase == null){

            $dbase = new Database();
            $dbase->connect();
            $this->dbase = $dbase;

        //die ($sql);

        }$retval = $this->dbase->select($sql);

        return $retval;
        
    }

}

?>