<?php

require_once "Repository.php";
require_once "RoomRepository.php";
require_once __DIR__.'\..\models\Room.php';
require_once __DIR__.'\..\models\Service.php';
require_once __DIR__.'\..\..\Database.php';

class ServiceRepository extends Repository {

    //private $dbase = null;

    public function getRawService(int $roomId, string $name): array {

        $sql = 'SELECT service.* 
                FROM service 
                INNER JOIN room ON service.room_id = room.room_id
                WHERE (service.room_id = "'.$roomId.'" )
                        AND (service.name = "'.$name.'")';

        if($this->dbase == null){

            $dbase = new Database();
            $dbase->connect();
            $this->dbase = $dbase;

        }//die($sql);

        $retval = $this->dbase->select($sql);
        $row = $retval->fetch_assoc();
        return $row;

    }


    public function insertService(Service $serv, Room $room) : int{

        //$roomRepo = new RoomRepository();
        //$rawRoom = $roomRepo->getRawRoom($_SESSION['id'], $roomName);

        //die("KOGO NAHUI");

        $sql = 'INSERT INTO service (room_id, value, name, type) VALUES ( "'.$room->getId().'", 
                                                                        "'.$serv->getValue().'", 
                                                                        "'.$serv->getName().'", 
                                                                        "'.$serv->getType().'")';

        if($this->dbase == null){

            $dbase = new Database();
            $dbase->connect();
            $this->dbase = $dbase;

        }

        $retval = $this->dbase->query($sql);

        $row = $this->getRawService($room->getId(), $serv->getName());

        return $row['service_id'];

    }

    public function updateService(Service $service){//check if user exists here

        //$rawRoom = $this->getRawRoom($_SESSION['id'], $oldName);

        $sql = 'UPDATE `service` 
                SET name = "'.$service->getName().'", value = "'.$service->getValue().'", type = "'.$service->getType().'" 
                WHERE service_id = "'.$service->getId().'"';

        $this->dbase->query($sql);//check if ok

        //die($sql);

        //update SERVICES

    }

    public function deleteService(Service $serv){//check if user exists here

        $sql = 'DELETE FROM service WHERE service.service_id = "'.$serv->getId().'"';

        $this->dbase->query($sql);

    }

    public function deleteAll(string $roomName){

        $roomRepo = new RoomRepository();
        $rawRoom = $roomRepo->getRawRoom($_SESSION['id'], $roomName);

        $sql = 'DELETE FROM service WHERE room_id = "'.$rawRoom['room_id'].'"';

        if($this->dbase == null){

            $dbase = new Database();
            $dbase->connect();
            $this->dbase = $dbase;

        }

        $retval = $this->dbase->query($sql);

    }

    public function getServices(string $email, string $roomName) {

        $sql = 'SELECT service.* 
                FROM service 
                INNER JOIN room ON service.room_id = room.room_id 
                WHERE room.room_id = ( SELECT room.room_id 
                                        FROM room 
                                        INNER JOIN user ON room.user_id = user.user_id 
                                        WHERE (user.email = "'.$email.'") 
                                                AND(room.name = "'.$roomName.'") )';

                                        

        if($this->dbase == null){

            $dbase = new Database();
            $dbase->connect();
            $this->dbase = $dbase;

        //die ($sql);

        }$retval = $this->dbase->select($sql);

        return $retval;
        
    }

}

?>