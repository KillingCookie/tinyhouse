<?php

    if(isset($_SESSION['id']) and isset($_SESSION['role'])) {
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/pai/?page=home");
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="icon" href="img/icon.ico" />
    <title>TinyHouse</title>
</head>
<body>
    <?php include(__DIR__.'/common/header.php'); ?>
    <div class="container">
        <div class="logo">
            <img src="img/tinyHouse.svg">
            <i class="TinyHouse">TinyHouse</i>
        </div>
        <form action="?page=index" method="POST">
            <p class="startText">Get all your household devices connected through the help of our service.</p>
            <button type="submit">Get Started</button>
        </form>
    </div>
</body>
</html>