<?php

    if(isset($_SESSION['id']) and isset($_SESSION['role'])) {
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/pai/?page=home");
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="icon" href="img/icon.ico" />
    <title>TinyHouse</title>
</head>
<body>
    <?php include(__DIR__.'\common\header.php'); ?>
    <div class="login-container">
        <div class="logo">
            <img src="img/tinyHouse.svg">
        </div>
        <?php if($_GET['page'] == 'register'){?>
            <form action="?page=register" method="POST">
                <?php
                    if(isset($messages)){
                        foreach($messages as $message) {
                            echo $message;
                        }
                    }
                ?>
                <input name="email" type="text" placeholder="example@email.com">
                <input name="password" type="password" placeholder="password">
                <input name="repeat_password" type="password" placeholder="repeat password">
                <input name="login" type="text" placeholder="login">
                <button type="submit" name="signup">Sign Up</button>
            </form><?php
        }else if($_GET['page'] == 'login'){?>
            <form action="?page=login" method="POST">
                <?php
                    if(isset($messages)){
                        foreach($messages as $message) {
                            echo $message;
                        }
                    }
                ?>
                <input name="email" type="text" placeholder="example@email.com">
                <input name="password" type="password" placeholder="password">
                <button type="submit" name="login">Log In</button>
                <button type="submit" name="signup">Sign Up</button>
            </form><?php
        }?>
    </div>
</body>
</html>