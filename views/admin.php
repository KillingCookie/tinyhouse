<?php

    if(!isset($_SESSION['role']) || $_SESSION['role'] != ['ROLE_ADMIN']) {
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/pai/?page=index");
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="icon" href="img/icon.ico" />
    <title>TinyHouse</title>
</head>
<body>
    <?php include(__DIR__.'\common\header.php'); ?>
    <div class="admin-container">
        <?php foreach($users as $key=>$user): ?>
            <form action="?page=admin" method="POST">
                <p><?= $user->getLogin() ?> , <?= $user->getEmail() ?></p>
                <button class="text-button" type="submit" name="delete-user-<?=$key?>">delete user</button>
             </form>
        <?php endforeach ?>
    </div>
</body>
</html>