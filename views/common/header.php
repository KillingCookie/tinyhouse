<header class="header">
    <div class="wrapper-main">
        <a href="?page=home">
            <i>TinyHouse</i>
        </a>
    </div>
    <div class="wrapper-other">
        <a class="simplea">language</a>
        <a class="simplea">settings</a>
        <?php if(isset($_SESSION['role']) && $_SESSION['role'] == ['ROLE_ADMIN']){

            ?> <a class="simplea" href="?page=admin">admin</a> <?php

        }
        
        if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
            ?> <a class="simplea" href="?page=login">log in</a> <?php
        } else{
            ?> <a class="simplea" href="?page=logout">log out</a> <?php
        }
        ?>
    </div>
</header>