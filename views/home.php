<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="icon" href="img/icon.ico" />
    <title>TinyHouse</title>
</head>
<body>
    <?php include(__DIR__.'/common/header.php'); ?>
    <?php if(isset($_GET['room'])): ?>
        <div class="tab">
            <div class="tab-contents">
                <!--<div class="image">-->
                    <img <?php if(isset($thisRoom)): ?> src="<?= "img/rooms/".$thisRoom->getImage().".png" ?>" <?php endif ?> >
                <!--</div>--->
                <div class="name">
                    <p><?=$thisRoom->getName()?></p>
                    <a href="?page=home">close</a>
                </div>
                <?php if(isset($_GET['edit'])): ?>
                    <form action="?page=home&room=<?= array_search($thisRoom, $rooms) ?>&edit=true" id="save-form" method="POST">
                            <button class="text-button" type="submit" name="save">save</button>
                            <button class="text-button" type="submit" name="delete">delete</button>
                            <div class="inputs">
                                <input class="inname" name="room-name" type="text" value="<?= $thisRoom->getName() ?>">
                                <select name="room-image">
                                    <option <?php if($thisRoom->getImage() == "bedroom1"): ?> <?= "selected" ?> <?php endif ?> 
                                        value="bedroom1">bedroom 1</option>
                                    <option <?php if($thisRoom->getImage() == "bedroom2"): ?> <?= "selected" ?> <?php endif ?> 
                                        value="bedroom2">bedroom 2</option>
                                    <option <?php if($thisRoom->getImage() == "bedroom3"): ?> <?= "selected" ?> <?php endif ?> 
                                        value="bedroom3">bedroom 3</option>
                                    <option <?php if($thisRoom->getImage() == "bathroom1"): ?> <?= "selected" ?> <?php endif ?> 
                                        value="bathroom1">bathroom 1</option>
                                    <option <?php if($thisRoom->getImage() == "bathroom2"): ?> <?= "selected" ?> <?php endif ?> 
                                        value="bathroom2">bathroom 2</option>
                                    <option <?php if($thisRoom->getImage() == "kitchen1"): ?> <?= "selected" ?> <?php endif ?> 
                                        value="kitchen1">kitchen 1</option>
                                    <option <?php if($thisRoom->getImage() == "kitchen2"): ?> <?= "selected" ?> <?php endif ?> 
                                        value="kitchen2">kitchen 2</option>
                                    <option <?php if($thisRoom->getImage() == "livingroom1"): ?> <?= "selected" ?> <?php endif ?> 
                                        value="livingroom1">living room 1</option>
                                    <option <?php if($thisRoom->getImage() == "livingroom2"): ?> <?= "selected" ?> <?php endif ?> 
                                        value="livingroom2">living room 2</option>
                                    <option <?php if($thisRoom->getImage() == "playroom1"): ?> <?= "selected" ?> <?php endif ?> 
                                        value="playroom1">playroom 1</option>
                                    </select>
                            </div>
                                        <!--</form>-->
                                <?php foreach($services as $key=>$service): ?>
                                        <!--<form action="" method="POST">-->
                                    <div class="inputs">
                                        <input class="names" name="service-name-<?= $key ?>" type="text" value="<?= $service->getName() ?>">
                                        <input class="values" name="service-value-<?= $key ?>" type="text" value="<?= $service->getValue() ?>">
                                        <select name="service-type-<?= $key ?>">
                                        <option <?php if($service->getType() == 1): ?> <?= "selected" ?> <?php endif ?> 
                                            value="number">Number</option>
                                        <option <?php if($service->getType() == 2): ?> <?= "selected" ?> <?php endif ?> 
                                            value="percentage">Percentage</option>
                                        <option <?php if($service->getType() == 3): ?> <?= "selected" ?> <?php endif ?> 
                                            value="temperature">Temperature</option>
                                        </select>
                                        <button class="del-ete" type="submit" name="service-delete-<?= $key ?>">delete</button>
                                    </div>
                                            <!--</form>-->
                                <?php endforeach ?>
                                            <!--<form action="" method="POST">--->
                            <div>
                                <button class="pl-us" type="submit" name="add-service">+</button>
                            </div>
                    </form>
                    <?php else: ?>
                        <?php if (array_search($thisRoom, $rooms) || array_search($thisRoom, $rooms) === 0): ?>
                        <form action="?page=home&room=<?= array_search($thisRoom, $rooms) ?>" id="edit-form" method="POST">
                            <button class="text-button" type="submit" name="edit-button">edit</button>
                        </form>
                            <?php else: ?>
                        <form action="?page=home&room=new" id="edit-form" method="POST">
                            <button class="text-button" type="submit" name="edit-button">edit</button>
                        </form>
                        <?php endif ?>
                    <?php endif ?>
                    </form>
                
                <?php if(!isset($_GET['edit'])): ?>
                    <div class="services">
                        <?php if(isset($thisRoom)): ?>
                            <?php foreach($services as $service): ?>
                                <?php if($service->getType() == 1): ?>
                                    <p><?= $service->getName() ?> : <?= $service->getValue() ?></p>
                                <?php elseif($service->getType() == 2): ?>
                                    <p><?= $service->getName() ?> : <?= $service->getValue() ?>%</p>
                                <?php elseif($service->getType() == 3): ?>
                                    <p><?= $service->getName() ?> : <?= $service->getValue() ?> &#8451;</p>
                                <?php endif ?>
                            <?php endforeach ?>
                        <?php endif ?>
                            </div>
                <?php else: ?>
                    <!--<form action="" method="POST">-->
                    
                    <!--</form>---->
                <?php endif ?>
            </div>
        </div>
    <?php endif ?>
    <div class="home-container">
        <div class="board">
            <?php foreach($rooms as $room): ?>
                <div class="wrapper">
                    <div class="action-bar">
                        <a class="room-name" href="?page=home&room=<?= array_search($room, $rooms) ?>"><?= $room->getName() ?></a>
                        <a class="edit" href="?page=home&room=<?= array_search($room, $rooms) ?>">Edit</a>
                    </div>
                    <div class="wrapper-img">
                        <a href="?page=home&room=<?= array_search($room, $rooms) ?>">
                            <img src="<?= "img/rooms/".$room->getImage().".png" ?>">
                        </a>
                    </div>
                </div>
            <?php endforeach ?>
            <div class="wrapper add-room">
                <div class="action-bar add-room">
                    <a class="add-room" href="?page=home&room=new">Add Room</a>
                    <a class="add-room" href=""></a>
                </div>
                <div class="wrapper-img add-room">
                    <a href="?page=home&room=new">
                        <img class="add-room" src="img/rooms/add.png">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php if(isset($_GET['room'])): ?>
        <script>

            tab = document.getElementsByClassName("tab")[0];

            tab.style.display = "block";

            container = document.getElementsByClassName("home-container")[0];//when everything is moving
            container.style.width = "70%";
            container.style.marginLeft = "27.5%";

        </script>
    <?php endif ?>
</body>
</html>