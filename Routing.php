<?php

require_once 'app\controllers\IndexController.php';
require_once 'app\controllers\LoginController.php';
require_once 'app\controllers\HomeController.php';
require_once 'app\controllers\AdminController.php';

class Routing {

    private $routes = [];

    public function __construct(){

        $this->routes = [
            'index' => ['controller' => 'IndexController', 'action' => 'index'],
            'login' => ['controller' => 'LoginController', 'action' => 'login'],
            'logout' => ['controller' => 'LoginController', 'action' => 'logout'],
            'register' => ['controller' => 'LoginController', 'action' => 'register'],
            'admin' => ['controller' => 'AdminController', 'action' => 'admin'],
            'home' => ['controller' => 'HomeController', 'action' => 'home']
        ];

    }

    public function run(){

        $page = isset($_GET['page']) ? $_GET['page'] : 'index';

        if (isset($this->routes[$page])) {
            $controller = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controller;
            $object->$action();

        }

    }

}

?>